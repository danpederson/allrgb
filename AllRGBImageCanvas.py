import math
from PIL import Image
import random

class AllRGBImageCanvas:
    def __init__(self,colorBits,sortingMethod,saveImageInterval):
        self.colorBits = colorBits
        self.blackPixelPosition = (-1,-1)
        self.saveImageInterval = saveImageInterval

        #Calculate total pixels based off of colorBits
        self.totalPixels = pow(pow(2,colorBits),3)

        #Calculate xLen and yLen
        if (colorBits%2) == 1:
            self.yLen = int(math.sqrt(self.totalPixels/2))
            self.xLen = int(2*self.yLen)
        else:
            self.yLen = int(math.sqrt(self.totalPixels))
            self.xLen = int(self.yLen)

        #Open up image
        self.img = Image.new('RGB',(self.xLen,self.yLen),"black")
        self.pixels = self.img.load()

        colorLimit = pow(2,self.colorBits)
        colorShift = 8-colorBits

        rs = [-1 for i in range(self.totalPixels)]
        gs = [-1 for i in range(self.totalPixels)]
        bs = [-1 for i in range(self.totalPixels)]
    
        index = 0

        for i in range(colorLimit):
            for j in range(colorLimit):
                for k in range(colorLimit):            
                    rs[index] = i<<colorShift
                    gs[index] = j<<colorShift
                    bs[index] = k<<colorShift

                    index = index + 1

        if (sortingMethod == 'Hue'):
            rgbSortTuple = [(rs[i],gs[i],bs[i], math.atan2(math.sqrt(3)*(float(gs[i])-float(bs[i])),2*float(rs[i])-float(gs[i])-float(bs[i]))) for i in range(self.totalPixels)]
        elif(sortingMethod == 'Luminance'):
            rgbSortTuple = [(rs[i],gs[i],bs[i],float(0.2126*rs[i]+0.7152*gs[i]+0.0722*bs[i])) for i in range(self.totalPixels)]
        else:
            rgbSortTuple = [(rs[i],gs[i],bs[i],0) for i in range(self.totalPixels)]
        del(rs)
        del(gs)
        del(bs)

        rgbSortTuple.sort(key = lambda sval:sval[3])
        self.rgbVals = [(rgbSortTuple[i][0],rgbSortTuple[i][1],rgbSortTuple[i][2]) for i in range(self.totalPixels)]
        self.ci = 0

    def PlacePixel(self,x,y,rgbVal):
        retVal = 0

        #If x or y are out of bounds, don't place a pixel.
        if ((x<0) or (x >= self.xLen) or (y < 0) or (y >= self.yLen)):        
            retVal = 0

        #If the x,y coordinates specify the placed black pixel, don't place a pixel
        elif ((x,y) == self.blackPixelPosition):
            retVal = 0

        #If the x,y coordinates do not specify an already-placed pixel position, place the pixel
        elif (self.pixels[x,y] == (0,0,0)):
            self.pixels[x,y] = (rgbVal)

            if (self.ci%self.saveImageInterval == 0):
                self.SaveCanvas(str(self.ci)+'.bmp')
                print('Saving ',self.ci)

            #If we just placed a black pixel, record it.
            if (rgbVal == (0,0,0)):
                self.blackPixelPosition = (x,y)

            retVal = 1
        
        return retVal

    def UnfilledPixelPosition(self,x,y):
        if ((x<0) or (x >= self.xLen) or (y < 0) or (y >= self.yLen)):        
            retVal = False

        #If the x,y coordinates specify the placed black pixel, don't place a pixel
        elif ((x,y) == self.blackPixelPosition):
            retVal = False

        elif (self.pixels[x,y] != (0,0,0)):
            retVal = False
        else:
            retVal = True
        return retVal

    def ValidPixelPosition(self,x,y):
        if ((x<0) or (x >= self.xLen) or (y < 0) or (y >= self.yLen)):        
            retVal = False
        else:
            retVal = True
        return retVal

    def ShowCanvas(self):
        self.img.show()

    def SaveCanvas(self,imgPath):
        self.img.save(imgPath)

    def AverageFilledNeighborColor_d(self,x,y,distance):
        #First, get a list of neighboring pixels
        rSum = 0
        bSum = 0
        gSum = 0
        summedPixels = 0
        
        for xi in range(x-distance,x+distance):
            for yj in range(y-distance,y+distance):
                if (math.sqrt(pow(xi-x,2) + pow(yj-y,2)) <= distance):
                    if ((not self.UnfilledPixelPosition(xi,yj)) and self.ValidPixelPosition(xi,yj)):
                        c = self.pixels[xi,yj]
                        rSum += c[0]
                        gSum += c[1]
                        bSum += c[2]
                        summedPixels += 1
                        
        if (summedPixels == 0):
            #print('No neighbors!')
            return(0,0,0)
        else:
            return (rSum/summedPixels,gSum/summedPixels,bSum/summedPixels)
        
        
    def AverageFilledNeighborColor(self,x,y):
        rSum = 0
        bSum = 0
        gSum = 0
        summedPixels = 0

        if ((x<0) or (x >= self.xLen) or (y < 0) or (y >= self.yLen)):        
            return (0,0,0)

        #Get W neighbor
        if (x > 0):
            if (not self.UnfilledPixelPosition(x-1,y)):
                c = self.pixels[x-1,y]
                rSum += c[0]
                gSum += c[1]
                bSum += c[2]
                summedPixels += 1
            
            #Get NW neighbor
            if (y > 0):
                if (not self.UnfilledPixelPosition(x-1,y-1)):
                    c = self.pixels[x-1,y-1]
                    rSum += c[0]
                    gSum += c[1]
                    bSum += c[2]
                    summedPixels += 1

            #Get SW neighbor
            if (y < self.yLen-1):
                if (not self.UnfilledPixelPosition(x-1,y+1)):
                    c = self.pixels[x-1,y+1]
                    rSum += c[0]
                    gSum += c[1]
                    bSum += c[2]
                    summedPixels += 1

        if (x < self.xLen-1):
            #Get E neighbor
            if (not self.UnfilledPixelPosition(x+1,y)):
                c = self.pixels[x+1,y]
                rSum += c[0]
                gSum += c[1]
                bSum += c[2]
                summedPixels += 1
                
            #Get NE neighbor
            if (y > 0):
                if (not self.UnfilledPixelPosition(x+1,y-1)):
                    c = self.pixels[x+1,y-1]
                    rSum += c[0]
                    gSum += c[1]
                    bSum += c[2]
                    summedPixels += 1

            #Get SE neighbor
            if (y < self.yLen-1):
                if (not self.UnfilledPixelPosition(x+1,y+1)):
                    c = self.pixels[x+1,y+1]
                    rSum += c[0]
                    gSum += c[1]
                    bSum += c[2]
                    summedPixels += 1

        #Get N neighbor
        if (y > 0):
            if (not self.UnfilledPixelPosition(x,y-1)):
                c = self.pixels[x,y-1]
                rSum += c[0]
                gSum += c[1]
                bSum += c[2]
                summedPixels += 1

        #Get S neighbor
        if (y < self.yLen-1):
            if (not self.UnfilledPixelPosition(x,y+1)):
                c = self.pixels[x,y+1]
                rSum += c[0]
                gSum += c[1]
                bSum += c[2]
                summedPixels += 1

        if (summedPixels == 0):
            print('No neighbors!')
            return(0,0,0)
        else:
            return (rSum/summedPixels,gSum/summedPixels,bSum/summedPixels)

    def AverageNeighborColor(self,x,y):
        rSum = 0
        bSum = 0
        gSum = 0
        summedPixels = 0
        
        if (x > 0):
            #Get W neighbor
            c = self.pixels[x-1,y]
            rSum += c[0]
            gSum += c[1]
            bSum += c[2]
            summedPixels += 1
            
            #Get NW neighbor
            if (y > 0):
                c = self.pixels[x-1,y-1]
                rSum += c[0]
                gSum += c[1]
                bSum += c[2]
                summedPixels += 1

            #Get SW neighbor
            if (y < self.yLen-1):
                c = self.pixels[x-1,y+1]
                rSum += c[0]
                gSum += c[1]
                bSum += c[2]
                summedPixels += 1

        if (x < self.xLen-1):
            #Get E neighbor
            c = self.pixels[x+1,y]
            rSum += c[0]
            gSum += c[1]
            bSum += c[2]
            summedPixels += 1
            
            #Get NE neighbor
            if (y > 0):
                c = self.pixels[x+1,y-1]
                rSum += c[0]
                gSum += c[1]
                bSum += c[2]
                summedPixels += 1

            #Get SE neighbor
            if (y < self.yLen-1):
                c = self.pixels[x+1,y+1]
                rSum += c[0]
                gSum += c[1]
                bSum += c[2]
                summedPixels += 1

        if (y > 0):
            #Get North Neighbor
            c = self.pixels[x,y-1]
            rSum += c[0]
            gSum += c[1]
            bSum += c[2]
            summedPixels += 1
        if (y < self.yLen-1):
            #Get South Neighbor
            c = self.pixels[x,y+1]
            rSum += c[0]
            gSum += c[1]
            bSum += c[2]
            summedPixels += 1

        return (rSum/summedPixels,gSum/summedPixels,bSum/summedPixels)

    
                
def Bresenham1(Canvas,x0,y0,x1,y1):
    
    #This is only good for the first octant (0)...need to switch octants based off of slope and direction.
    #Octants are as follows:
    #
    #\2|1/
    #3\|/0
    #--+--
    #4/|\7
    #/5|6\
    #
    octant = -1

    #We need to figure out what octant we're in!
    if (x1 > x0):
        if (y1 > y0):
            if (abs(x1-x0)>abs(y1-y0)):
                octant = 7
            else:
                octant = 6
        else:
            if (abs(x1-x0)>abs(y1-y0)):
                octant = 0
            else:
                octant = 1
    else:
        if (y1>y0):
            if (abs(x1-x0) > abs(y1-y0)):
                octant = 4
            else:
                octant = 5
        else:
            if (abs(x1-x0) > abs(y1-y0)):
                octant = 3
            else:
                octant = 2
    
    deltax = abs(x1-x0)
    deltay = abs(y1-y0)

    if ((deltay == 0) or (deltax == 0)):
        #draw vert. line
        if (deltay == 0):
            if(x1>x0):
                for x in range(x0,x1+1):
                    Canvas.ci += Canvas.PlacePixel(x,y1,Canvas.rgbVals[Canvas.ci])
            else:
                for x in range(x1,x0+1):
                    Canvas.ci += Canvas.PlacePixel(x,y1,Canvas.rgbVals[Canvas.ci])
        else:
            if (y1>y0):
                for y in range(y0,y1+1):
                    Canvas.ci += Canvas.PlacePixel(x1,y,Canvas.rgbVals[Canvas.ci])
            else:
                for y in range(y1,y0+1):
                    Canvas.ci += Canvas.PlacePixel(x1,y,Canvas.rgbVals[Canvas.ci])
                
    else:
        if (octant == 6):
            error = 0
            deltaerr = abs(deltax/deltay) #Assumes not a vertical line. Can manually draw a vert line if need be
            loopVarStart = y0
            loopVarEnd = y1
            e = x0
        elif (octant == 5):
            error = 0
            deltaerr = abs(deltax/deltay) #Assumes not a vertical line. Can manually draw a vert line if need be
            loopVarStart = y0
            loopVarEnd = y1
            e = x1
        elif (octant == 1):
            error = 0
            deltaerr = abs(deltax/deltay) #Assumes not a vertical line. Can manually draw a vert line if need be
            loopVarStart = y1
            loopVarEnd = y0
            e = x0
        elif (octant == 2):
            error = 0
            deltaerr = abs(deltax/deltay) #Assumes not a vertical line. Can manually draw a vert line if need be
            loopVarStart = y1
            loopVarEnd = y0
            e = x1
        elif ((octant == 3) or (octant == 4)):
            error = 0
            deltaerr = abs(deltay/deltax)
            loopVarStart = x1
            loopVarEnd = x0
            e = y0       
        else:
            error = 0
            deltaerr = abs(deltay/deltax) #Assumes not a vertical line. Can manually draw a vert line if need be
            loopVarStart = x0
            loopVarEnd = x1
            e = y0

        for l in range (loopVarStart,loopVarEnd+1):
            if octant == 0:
                Canvas.ci += Canvas.PlacePixel(l,2*y0-e,Canvas.rgbVals[Canvas.ci])
            elif octant == 1:
                Canvas.ci += Canvas.PlacePixel(e,y0-l+y1,Canvas.rgbVals[Canvas.ci])
            elif octant == 2:
                Canvas.ci += Canvas.PlacePixel(x0+x1-e,y0-l+y1,Canvas.rgbVals[Canvas.ci])
            elif octant == 3:
                Canvas.ci += Canvas.PlacePixel(l,e-(y0-y1),Canvas.rgbVals[Canvas.ci])
            elif octant == 4:
                Canvas.ci += Canvas.PlacePixel(l,y1+y0-e,Canvas.rgbVals[Canvas.ci])
            elif octant == 5:
                Canvas.ci += Canvas.PlacePixel(x0+x1-e,l,Canvas.rgbVals[Canvas.ci])
            elif octant == 6:
                Canvas.ci += Canvas.PlacePixel(e,l,Canvas.rgbVals[Canvas.ci])
            elif octant == 7:
                Canvas.ci += Canvas.PlacePixel(l,e,Canvas.rgbVals[Canvas.ci])
                
            error = error + deltaerr
            if error >= 0.5:
                e = e+1
                error = error-1.0

def Bresenham2(Canvas,availableColors,x0,y0,x1,y1):
    #This is only good for the first octant (0)...need to switch octants based off of slope and direction.
    #Octants are as follows:
    #
    #\2|1/
    #3\|/0
    #--+--
    #4/|\7
    #/5|6\
    #
    octant = -1

    #We need to figure out what octant we're in!
    if (x1 > x0):
        if (y1 > y0):
            if (abs(x1-x0)>abs(y1-y0)):
                octant = 7
            else:
                octant = 6
        else:
            if (abs(x1-x0)>abs(y1-y0)):
                octant = 0
            else:
                octant = 1
    else:
        if (y1>y0):
            if (abs(x1-x0) > abs(y1-y0)):
                octant = 4
            else:
                octant = 5
        else:
            if (abs(x1-x0) > abs(y1-y0)):
                octant = 3
            else:
                octant = 2
    
    deltax = abs(x1-x0)
    deltay = abs(y1-y0)

    if ((deltay == 0) or (deltax == 0)):
        #draw vert. line
        if (deltay == 0):
            if(x1>x0):
                for x in range(x0,x1+1):
                    ci = Canvas.ci
                    Canvas.ci += Canvas.PlacePixel(x,y1,Canvas.rgbVals[Canvas.ci])
                    if (ci != Canvas.ci):
                        availableColors.DeactivateColorNode(Canvas.rgbVals[Canvas.ci-1])
            else:
                for x in range(x1,x0+1):
                    ci = Canvas.ci
                    Canvas.ci += Canvas.PlacePixel(x,y1,Canvas.rgbVals[Canvas.ci])
                    if (ci != Canvas.ci):
                        availableColors.DeactivateColorNode(Canvas.rgbVals[Canvas.ci-1])
        else:
            if (y1>y0):
                for y in range(y0,y1+1):
                    ci = Canvas.ci
                    Canvas.ci += Canvas.PlacePixel(x1,y,Canvas.rgbVals[Canvas.ci])
                    if (ci != Canvas.ci):
                        availableColors.DeactivateColorNode(Canvas.rgbVals[Canvas.ci-1])
            else:
                for y in range(y1,y0+1):
                    ci = Canvas.ci
                    Canvas.ci += Canvas.PlacePixel(x1,y,Canvas.rgbVals[Canvas.ci])
                    if (ci != Canvas.ci):
                        availableColors.DeactivateColorNode(Canvas.rgbVals[Canvas.ci-1])
                
    else:
        if (octant == 6):
            error = 0
            deltaerr = abs(deltax/deltay) #Assumes not a vertical line. Can manually draw a vert line if need be
            loopVarStart = y0
            loopVarEnd = y1
            e = x0
        elif (octant == 5):
            error = 0
            deltaerr = abs(deltax/deltay) #Assumes not a vertical line. Can manually draw a vert line if need be
            loopVarStart = y0
            loopVarEnd = y1
            e = x1
        elif (octant == 1):
            error = 0
            deltaerr = abs(deltax/deltay) #Assumes not a vertical line. Can manually draw a vert line if need be
            loopVarStart = y1
            loopVarEnd = y0
            e = x0
        elif (octant == 2):
            error = 0
            deltaerr = abs(deltax/deltay) #Assumes not a vertical line. Can manually draw a vert line if need be
            loopVarStart = y1
            loopVarEnd = y0
            e = x1
        elif ((octant == 3) or (octant == 4)):
            error = 0
            deltaerr = abs(deltay/deltax)
            loopVarStart = x1
            loopVarEnd = x0
            e = y0       
        else:
            error = 0
            deltaerr = abs(deltay/deltax) #Assumes not a vertical line. Can manually draw a vert line if need be
            loopVarStart = x0
            loopVarEnd = x1
            e = y0

        for l in range (loopVarStart,loopVarEnd+1):
            if octant == 0:
                ci = Canvas.ci
                Canvas.ci += Canvas.PlacePixel(l,2*y0-e,Canvas.rgbVals[Canvas.ci])
                if (ci != Canvas.ci):
                    availableColors.DeactivateColorNode(Canvas.rgbVals[Canvas.ci-1])
            elif octant == 1:
                ci = Canvas.ci
                Canvas.ci += Canvas.PlacePixel(e,y0-l+y1,Canvas.rgbVals[Canvas.ci])
                if (ci != Canvas.ci):
                    availableColors.DeactivateColorNode(Canvas.rgbVals[Canvas.ci-1])
            elif octant == 2:
                ci = Canvas.ci
                Canvas.ci += Canvas.PlacePixel(x0+x1-e,y0-l+y1,Canvas.rgbVals[Canvas.ci])
                if (ci != Canvas.ci):    
                    availableColors.DeactivateColorNode(Canvas.rgbVals[Canvas.ci-1])
            elif octant == 3:
                ci = Canvas.ci
                Canvas.ci += Canvas.PlacePixel(l,e-(y0-y1),Canvas.rgbVals[Canvas.ci])
                if (ci != Canvas.ci):
                    availableColors.DeactivateColorNode(Canvas.rgbVals[Canvas.ci-1])
            elif octant == 4:
                ci = Canvas.ci
                Canvas.ci += Canvas.PlacePixel(l,y1+y0-e,Canvas.rgbVals[Canvas.ci])
                if (ci != Canvas.ci):
                    availableColors.DeactivateColorNode(Canvas.rgbVals[Canvas.ci-1])
            elif octant == 5:
                ci = Canvas.ci
                Canvas.ci += Canvas.PlacePixel(x0+x1-e,l,Canvas.rgbVals[Canvas.ci])
                if (ci != Canvas.ci):
                    availableColors.DeactivateColorNode(Canvas.rgbVals[Canvas.ci-1])
            elif octant == 6:
                ci = Canvas.ci
                Canvas.ci += Canvas.PlacePixel(e,l,Canvas.rgbVals[Canvas.ci])
                if (ci != Canvas.ci):
                    availableColors.DeactivateColorNode(Canvas.rgbVals[Canvas.ci-1])
            elif octant == 7:
                ci = Canvas.ci
                Canvas.ci += Canvas.PlacePixel(l,e,Canvas.rgbVals[Canvas.ci])
                if (ci != Canvas.ci):
                    availableColors.DeactivateColorNode(Canvas.rgbVals[Canvas.ci-1])
                
            error = error + deltaerr
            if error >= 0.5:
                e = e+1
                error = error-1.0

def Bresenham(Canvas,availableColors,x0,y0,x1,y1,neighborDistance):
    
    #This is only good for the first octant (0)...need to switch octants based off of slope and direction.
    #Octants are as follows:
    #
    #\2|1/
    #3\|/0
    #--+--
    #4/|\7
    #/5|6\
    #
    octant = -1

    #We need to figure out what octant we're in!
    if (x1 > x0):
        if (y1 > y0):
            if (abs(x1-x0)>abs(y1-y0)):
                octant = 7
            else:
                octant = 6
        else:
            if (abs(x1-x0)>abs(y1-y0)):
                octant = 0
            else:
                octant = 1
    else:
        if (y1>y0):
            if (abs(x1-x0) > abs(y1-y0)):
                octant = 4
            else:
                octant = 5
        else:
            if (abs(x1-x0) > abs(y1-y0)):
                octant = 3
            else:
                octant = 2
    
    deltax = abs(x1-x0)
    deltay = abs(y1-y0)

    if ((deltay == 0) or (deltax == 0)):
        #draw vert. line
        if (deltay == 0):
            if(x1>x0):
                for x in range(x0,x1+1):
                    xind = x
                    yind = y1
                    canvas = Canvas
                    if (canvas.UnfilledPixelPosition(xind,yind)):    
                        avgColor = canvas.AverageFilledNeighborColor_d(xind,yind,neighborDistance)
                        nearestColor = availableColors.FindNearestColor_ByHue((round(avgColor[0]),round(avgColor[1]),round(avgColor[2])))

                        if (not nearestColor == (0,0,0)):
                            if(canvas.PlacePixel(xind,yind,nearestColor)):
                                canvas.ci += 1
                                availableColors.DeactivateColorNode(nearestColor)
                        else:
                            randInd = random.randint(0,canvas.totalPixels-1)
                            nearestColor = availableColors.FindNearestColor_ByHue(canvas.rgbVals[randInd])
                            if(canvas.PlacePixel(xind,yind,nearestColor)):
                                canvas.ci += 1
                                availableColors.DeactivateColorNode(nearestColor)

                    #Canvas.ci += Canvas.PlacePixel(x,y1,Canvas.rgbVals[Canvas.ci])
            else:
                for x in range(x1,x0+1):
                    xind = x
                    yind = y1
                    canvas = Canvas
                    if (canvas.UnfilledPixelPosition(xind,yind)):    
                        avgColor = canvas.AverageFilledNeighborColor_d(xind,yind,neighborDistance)
                        nearestColor = availableColors.FindNearestColor_ByHue((round(avgColor[0]),round(avgColor[1]),round(avgColor[2])))

                        if (not nearestColor == (0,0,0)):
                            if(canvas.PlacePixel(xind,yind,nearestColor)):
                                canvas.ci += 1
                                availableColors.DeactivateColorNode(nearestColor)
                        else:
                            randInd = random.randint(0,canvas.totalPixels-1)
                            nearestColor = availableColors.FindNearestColor_ByHue(canvas.rgbVals[randInd])
                            if(canvas.PlacePixel(xind,yind,nearestColor)):
                                canvas.ci += 1
                                availableColors.DeactivateColorNode(nearestColor)
                    #Canvas.ci += Canvas.PlacePixel(x,y1,Canvas.rgbVals[Canvas.ci])
        else:
            if (y1>y0):
                for y in range(y0,y1+1):
                    xind = x1
                    yind = y
                    canvas = Canvas
                    if (canvas.UnfilledPixelPosition(xind,yind)):    
                        avgColor = canvas.AverageFilledNeighborColor_d(xind,yind,neighborDistance)
                        nearestColor = availableColors.FindNearestColor_ByHue((round(avgColor[0]),round(avgColor[1]),round(avgColor[2])))

                        if (not nearestColor == (0,0,0)):
                            if(canvas.PlacePixel(xind,yind,nearestColor)):
                                canvas.ci += 1
                                availableColors.DeactivateColorNode(nearestColor)
                        else:
                            randInd = random.randint(0,canvas.totalPixels-1)
                            nearestColor = availableColors.FindNearestColor_ByHue(canvas.rgbVals[randInd])
                            if(canvas.PlacePixel(xind,yind,nearestColor)):
                                canvas.ci += 1
                                availableColors.DeactivateColorNode(nearestColor)
                    #Canvas.ci += Canvas.PlacePixel(x1,y,Canvas.rgbVals[Canvas.ci])
            else:
                for y in range(y1,y0+1):
                    xind = x1
                    yind = y
                    canvas = Canvas
                    if (canvas.UnfilledPixelPosition(xind,yind)):    
                        avgColor = canvas.AverageFilledNeighborColor_d(xind,yind,neighborDistance)
                        nearestColor = availableColors.FindNearestColor_ByHue((round(avgColor[0]),round(avgColor[1]),round(avgColor[2])))

                        if (not nearestColor == (0,0,0)):
                            if(canvas.PlacePixel(xind,yind,nearestColor)):
                                canvas.ci += 1
                                availableColors.DeactivateColorNode(nearestColor)
                        else:
                            randInd = random.randint(0,canvas.totalPixels-1)
                            nearestColor = availableColors.FindNearestColor_ByHue(canvas.rgbVals[randInd])
                            if(canvas.PlacePixel(xind,yind,nearestColor)):
                                canvas.ci += 1
                                availableColors.DeactivateColorNode(nearestColor)
                    #Canvas.ci += Canvas.PlacePixel(x1,y,Canvas.rgbVals[Canvas.ci])

                
    else:
        if (octant == 6):
            error = 0
            deltaerr = abs(deltax/deltay) #Assumes not a vertical line. Can manually draw a vert line if need be
            loopVarStart = y0
            loopVarEnd = y1
            e = x0
        elif (octant == 5):
            error = 0
            deltaerr = abs(deltax/deltay) #Assumes not a vertical line. Can manually draw a vert line if need be
            loopVarStart = y0
            loopVarEnd = y1
            e = x1
        elif (octant == 1):
            error = 0
            deltaerr = abs(deltax/deltay) #Assumes not a vertical line. Can manually draw a vert line if need be
            loopVarStart = y1
            loopVarEnd = y0
            e = x0
        elif (octant == 2):
            error = 0
            deltaerr = abs(deltax/deltay) #Assumes not a vertical line. Can manually draw a vert line if need be
            loopVarStart = y1
            loopVarEnd = y0
            e = x1
        elif ((octant == 3) or (octant == 4)):
            error = 0
            deltaerr = abs(deltay/deltax)
            loopVarStart = x1
            loopVarEnd = x0
            e = y0       
        else:
            error = 0
            deltaerr = abs(deltay/deltax) #Assumes not a vertical line. Can manually draw a vert line if need be
            loopVarStart = x0
            loopVarEnd = x1
            e = y0

        for l in range (loopVarStart,loopVarEnd+1):
            if octant == 0:
                xind = l
                yind = 2*y0-e
                #Canvas.ci += Canvas.PlacePixel(l,2*y0-e,Canvas.rgbVals[Canvas.ci])
            elif octant == 1:
                xind = e
                yind = y0-l+y1
                #Canvas.ci += Canvas.PlacePixel(e,y0-l+y1,Canvas.rgbVals[Canvas.ci])
            elif octant == 2:
                xind = x0+x1-e
                yind = y0-l+y1
                #Canvas.ci += Canvas.PlacePixel(x0+x1-e,y0-l+y1,Canvas.rgbVals[Canvas.ci])
            elif octant == 3:
                xind = l
                yind = e-(y0-y1)
                #Canvas.ci += Canvas.PlacePixel(l,e-(y0-y1),Canvas.rgbVals[Canvas.ci])
            elif octant == 4:
                xind = l
                yind = y1+y0-e
                #Canvas.ci += Canvas.PlacePixel(l,y1+y0-e,Canvas.rgbVals[Canvas.ci])
            elif octant == 5:
                xind = x0+x1-e
                yind = l
                #Canvas.ci += Canvas.PlacePixel(x0+x1-e,l,Canvas.rgbVals[Canvas.ci])
            elif octant == 6:
                xind = e
                yind = l
                #Canvas.ci += Canvas.PlacePixel(e,l,Canvas.rgbVals[Canvas.ci])
            elif octant == 7:
                xind = l
                yind = e
                #Canvas.ci += Canvas.PlacePixel(l,e,Canvas.rgbVals[Canvas.ci])
            
            canvas = Canvas
            if (canvas.UnfilledPixelPosition(xind,yind)):    
                avgColor = canvas.AverageFilledNeighborColor_d(xind,yind,neighborDistance)
                nearestColor = availableColors.FindNearestColor_ByHue((round(avgColor[0]),round(avgColor[1]),round(avgColor[2])))

                if (not nearestColor == (0,0,0)):
                    if(canvas.PlacePixel(xind,yind,nearestColor)):
                        canvas.ci += 1
                        availableColors.DeactivateColorNode(nearestColor)
                else:
                    randInd = random.randint(0,canvas.totalPixels-1)
                    nearestColor = availableColors.FindNearestColor_ByHue(canvas.rgbVals[randInd])
                    if(canvas.PlacePixel(xind,yind,nearestColor)):
                        canvas.ci += 1
                        availableColors.DeactivateColorNode(nearestColor)

            error = error + deltaerr
            if error >= 0.5:
                e = e+1
                error = error-1.0
                
def DrawLine(Canvas, x0,y0,x1,y1,thickness):
    if (abs(x0-x1) < abs(y0-y1)):
        for xOffset in range(int(-thickness/2),int(thickness/2)+1):
            Bresenham1(Canvas, x0+xOffset,y0,x1+xOffset,y1)
    else:
        for yOffset in range(int(-thickness/2),int(thickness/2)+1):
            Bresenham1(Canvas, x0,y0+yOffset,x1,y1+yOffset)

def DrawRecursiveTreeBranch(Canvas, x0,y0,length,currentTheta,theta,lengthDivider):
    #Draw branch 1
    x1 = int(length*math.cos(currentTheta + theta)+x0)
    y1 = int(length*math.sin(currentTheta + theta)+y0)
    DrawLine(Canvas, x0,y0,x1,y1,int(length/10)+1)
    #Recursively draw branches for new branch 1
    if (length >= 1):
        DrawRecursiveTreeBranch(Canvas, x1,y1,int(length/lengthDivider), currentTheta + theta, theta, lengthDivider)

    #Draw branch 2
    x1 = int(length*math.cos(currentTheta - theta)+x0)
    y1 = int(length*math.sin(currentTheta - theta)+y0)
    DrawLine(Canvas, x0,y0,x1,y1,int(length/10)+1)
    #Recursively draw branches for new branch 2
    if (length >= 1):
        DrawRecursiveTreeBranch(Canvas, x1,y1,int(length/lengthDivider), currentTheta - theta, theta, lengthDivider)

def FloodFill_Recursive(Canvas, x, y):
    if (Canvas.PlacePixel(x,y,Canvas.rgbVals[Canvas.ci]) == 1):
        AllColors.ci += 1
        FloodFill(Canvas,x+1,y)
        FloodFill(Canvas,x-1,y)
        FloodFill(Canvas,x,y+1)
        FloodFill(Canvas,x,y-1)

def FloodFill_Queue(Canvas, x, y):
    Q = [(x,y)]
    ProcessedCoords = [[False for i in range(Canvas.xLen)] for j in range(Canvas.yLen)]
    numiters = 0
    while(len(Q) > 0):
        numiters += 1
        n = Q.pop()
        if (Canvas.PlacePixel(n[0],n[1],Canvas.rgbVals[Canvas.ci]) == 1):
            Canvas.ci += 1

            #Randomize how things are added to Q
            direction = [1,2,3,4]
            random.shuffle(direction)
            for i in direction:
                if (i == 1):
                    if ((n[0] + 1) < Canvas.xLen):
                        if (not ProcessedCoords[n[0]+1][n[1]]):
                            Q.insert(0,(n[0]+1,n[1]))
                elif (i == 2):
                    if ((n[0] - 1) >= 0):
                        if (not ProcessedCoords[n[0]-1][n[1]]):
                            Q.insert(0,(n[0]-1,n[1]))
                elif(i == 3):
                    if ((n[1] + 1) < Canvas.yLen):
                        if (not ProcessedCoords[n[0]][n[1]+1]):
                            Q.insert(0,(n[0],n[1]+1))
                elif (i == 4):
                    if ((n[1] - 1) >= 0):
                        if (not ProcessedCoords[n[0]][n[1]-1]):
                            Q.insert(0,(n[0],n[1]-1))

def FloodFill_Stack(Canvas, x, y):
    Q = [(x,y)]

    if (not Canvas.UnfilledPixelPosition(x,y)):
        return
    
    ProcessedCoords = [[False for i in range(Canvas.xLen)] for j in range(Canvas.yLen)]
    numiters = 0
    while(len(Q) > 0):
        numiters += 1
        n = Q.pop()
        if (Canvas.ci == len(Canvas.rgbVals)):
            break
        if (Canvas.PlacePixel(n[0],n[1],Canvas.rgbVals[Canvas.ci]) == 1):
            Canvas.ci += 1
            
            #Randomize how things are added to Q
            direction = [1,2,3,4]
            random.shuffle(direction)
            for i in direction:
                if (i == 1):
                    if ((n[0] + 1) < Canvas.xLen):
                        if (not ProcessedCoords[n[0]+1][n[1]]):
                            Q.append((n[0]+1,n[1]))
                elif (i == 2):
                    if ((n[0] - 1) >= 0):
                        if (not ProcessedCoords[n[0]-1][n[1]]):
                            Q.append((n[0]-1,n[1]))
                elif(i == 3):
                    if ((n[1] + 1) < Canvas.yLen):
                        if (not ProcessedCoords[n[0]][n[1]+1]):
                            Q.append((n[0],n[1]+1))

                elif (i == 4):
                    if ((n[1] - 1) >= 0):
                        if (not ProcessedCoords[n[0]][n[1]-1]):
                            Q.append((n[0],n[1]-1))
