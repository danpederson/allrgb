import AllRGBImageCanvas
import math
import random

def makeChildren(targetNode,depth):
    cl = [(0,0,0),(0,0,1),(0,1,0),(0,1,1),(1,0,0),(1,0,1),(1,1,0),(1,1,1)] 
    
    if (depth == 1):
        targetNode.children = [leaf(cl[j],targetNode,True) for j in range(8)]
    else:
        targetNode.children = [node(cl[j],targetNode) for j in range(8)]
    
    if (depth > 1):
        for child in range (8):
            makeChildren(targetNode.children[child],depth-1)

def getActiveSiblingNodes(targetNode):
    retVal = []
    for i in range(8):
        n = targetNode.parent.children[i]
        if (n.active):
            retVal.append(n)
    return retVal

def deactivateColorNode(targetNode):
    targetNode.active = False
    activeSiblings = getActiveSiblingNodes(targetNode)
    if (len(activeSiblings) == 0):
        if (not(targetNode.value == 'root')):
            deactivateColorNode(targetNode.parent)

def getColorNode(tree,color,levelNum):
    if (not (levelNum == -1)):
        childIndex = ((color[0]>>levelNum)<<2) + ((color[1]>>levelNum)<<1) + (color[2]>>levelNum)
        newColor = (color[0]&~(1<<levelNum),color[1]&~(1<<levelNum),color[2]&~(1<<levelNum))             
        return getColorNode(tree.children[childIndex],newColor,levelNum-1)
    else:
        return tree
    
def getLeafColorLevels(leaf):
    nd = leaf
    levels = []
    while(not(nd.value == 'root')):
        levels.append(nd.value)
        nd = nd.parent
    return levels

def valueDistance(value1,value2):
    return (value1[0]^value2[0]) + (value1[1]^value2[1]) + (value1[2]^value2[2])

def siblingDistance(targetNode,siblingNode):
    return valueDistance(targetNode.value,siblingNode.value)

def findNearestActiveSiblingNodes(targetNode):
    activeSiblings = getActiveSiblingNodes(targetNode)
    shortestDistance = 4
    retVal = [shortestDistance]
    
    for i in range(len(activeSiblings)):
        dist = siblingDistance(targetNode,activeSiblings[i])
        if (dist < shortestDistance):
            shortestDistance = dist
            retVal = [shortestDistance,activeSiblings[i]]
        elif (dist == shortestDistance):
            retVal.append(activeSiblings[i])
    
    return retVal


def getActiveChildrenNodes(targetNode):
    retVal = []
    for i in range(8):
        n = targetNode.children[i]
        if (n.active):
            retVal.append(n)
    return retVal

def findNearestActiveChildNodes(value,targetNode):
    activeChildren = getActiveChildrenNodes(targetNode)
    shortestDistance = 4
    retVal = [shortestDistance]
    
    for i in range(len(activeChildren)):
        dist = valueDistance(value,activeChildren[i].value)
        if (dist < shortestDistance):
            shortestDistance = dist
            retVal = [shortestDistance,activeChildren[i]]
        elif (dist == shortestDistance):
            retVal.append(activeChildren[i])
    
    return retVal

def getRGBVal(colorLeaf):
    levels = getLeafColorLevels(colorLeaf)
    r = 0
    g = 0
    b = 0
    for i in range(len(levels)):
        r += ((levels[i][0])<<i)
        g += ((levels[i][1])<<i)
        b += ((levels[i][2])<<i)
    return (r,g,b)

def findNearestActiveNodes(targetColorNode):
    #First, we need to go up the line until we find active siblings
    level = 0
    levels = getLeafColorLevels(targetColorNode)
    nd = targetColorNode
    activeSiblings = findNearestActiveSiblingNodes(nd)
    
    #Go up the chain until we find active siblings
    while (len(activeSiblings) == 1):
        level = level + 1
        nd = nd.parent
        activeSiblings = findNearestActiveSiblingNodes(nd)
    
    #Now, we have a list of nodes that contain possibly a LOT of colors.
    #Traverse the list, collecting the colors that are nearest.
    activeSiblings = activeSiblings[1:len(activeSiblings)]
    while (level > 0):
        nodeList = []
        dist = 4
        for i in range(len(activeSiblings)):
            activeChildren = findNearestActiveChildNodes(levels[level-1],activeSiblings[i])
            newDist = activeChildren[0]
            #print(newDist)
            if (newDist < dist):
                nodeList = []
                nodeList.extend(activeChildren[1:len(activeChildren)])
                dist = newDist
            elif (dist == newDist):
                nodeList.extend(activeChildren[1:len(activeChildren)])
        activeSiblings = nodeList
        level = level-1
        
    return activeSiblings

class root(object):
    def __init__(self, children = [], depth = 1):
        self.value = 'root'
        self.children = children
        self.parent = self
        self.active = True
        self.depth = depth

        makeChildren(self,depth)
        
    def __repr__(self, level=0):
        ret = "\t"*level+repr(self.value)+"\n"
        for child in self.children:
            ret += child.__repr__(level+1)
        return ret

    def DeactivateColorNode(self, rgbVal):
        shiftedColor = (rgbVal[0]>>(8-self.depth),rgbVal[1]>>(8-self.depth),rgbVal[2]>>(8-self.depth))
        nd = getColorNode(self,shiftedColor,self.depth-1)
        deactivateColorNode(nd)

    def FindNearestColors(self, rgbVal):
        shiftedColor = (rgbVal[0]>>(8-self.depth),rgbVal[1]>>(8-self.depth),rgbVal[2]>>(8-self.depth))
        shiftedColorNode = getColorNode(self,shiftedColor,self.depth-1)
        nearestColorNodes = findNearestActiveNodes(shiftedColorNode)

        nearestColors = [(0,0,0) for i in range(len(nearestColorNodes))]
        for i in range(len(nearestColorNodes)):
            c = getRGBVal(nearestColorNodes[i])
            nearestColors[i] = (c[0]<<(8-self.depth),c[1]<<(8-self.depth),c[2]<<(8-self.depth))
        return nearestColors

    def FindNearestColor_ByHue(self, rgbVal):
        nearestColors = self.FindNearestColors(rgbVal)
        colorHues = [0 for i in range(len(nearestColors))]
        for i in range(len(nearestColors)):
            r = nearestColors[i][0]
            g = nearestColors[i][1]
            b = nearestColors[i][2]
            colorHues[i] = math.atan2(math.sqrt(3)*(float(g)-float(b)),2*float(r)-float(g)-float(b)) 

        colorHue = math.atan2(math.sqrt(3)*(float(rgbVal[1])-float(rgbVal[2])),2*float(rgbVal[0])-float(rgbVal[1])-float(rgbVal[2]))
        hueDiff = abs(colorHue - colorHues[0])
        nearestColor = nearestColors[0]
        nearestIndex = 0

        for i in range(len(nearestColors)):
            hd = abs(colorHue - colorHues[i])
            if (hd < hueDiff):
                hueDiff = hd
                nearestColor = nearestColors[i]
                
        return nearestColor

class node(object):
    def __init__(self, value, parent, children = []):
        self.value = value
        self.children = children
        self.parent = parent
        self.active = True
        
    def __repr__(self, level=0):
        ret = "\t"*level+repr(self.value)+"\n"
        for child in self.children:
            ret += child.__repr__(level+1)
        return ret

class leaf(object):
    def __init__(self,value,parent,active):
        self.value = value
        self.active = active
        self.parent = parent
        self.children = []
    
    def __repr__(self, level=0):
        ret = "\t"*level+repr(self.value)+"\n"
        return ret

def FloodFill_Stack(Canvas, x, y):
    global availableColors
    Q = [(x,y)]

    if (not Canvas.UnfilledPixelPosition(x,y)):
        return
    
    ProcessedCoords = [[False for i in range(Canvas.xLen)] for j in range(Canvas.yLen)]
    numiters = 0
    while(len(Q) > 0):
        numiters += 1
        n = Q.pop()
        if (Canvas.ci == len(Canvas.rgbVals)):
            break

        avgColor = Canvas.AverageFilledNeighborColor(n[0],n[1])
        #Get nearest real color
        realColor = (round(avgColor[0]),round(avgColor[1]),round(avgColor[2]))
        realColor = (realColor[0]>>(8-Canvas.colorBits),realColor[1]>>(8-Canvas.colorBits),realColor[2]>>(8-Canvas.colorBits))
        realColorNode = getColorNode(availableColors,realColor,Canvas.colorBits-1)
        nearestColorNodes = findNearestActiveNodes(realColorNode)

        nearestColors = [(0,0,0) for i in range(len(nearestColorNodes))]
        for i in range(len(nearestColorNodes)):
            c = getRGBVal(nearestColorNodes[i])
            nearestColors[i] = (c[0]<<(8-Canvas.colorBits),c[1]<<(8-Canvas.colorBits),c[2]<<(8-Canvas.colorBits))
            #nearestColors[i][0] = c[0]<<(8-Canvas.colorBits)
            #nearestColors[i][1] = c[1]<<(8-Canvas.colorBits)
            #nearestColors[i][2] = c[2]<<(8-Canvas.colorBits)
        
        #nearestColor = getRGBVal(nearestColorNodes[0])
        #nearestColor = (nearestColor[0]<<(8-Canvas.colorBits),nearestColor[1]<<(8-Canvas.colorBits),nearestColor[2]<<(8-Canvas.colorBits))
        colorHues = [0 for i in range(len(nearestColors))]
        for i in range(len(nearestColorNodes)):
            r = nearestColors[i][0]
            g = nearestColors[i][1]
            b = nearestColors[i][2]
            colorHues[i] = math.atan2(math.sqrt(3)*(float(g)-float(b)),2*float(r)-float(g)-float(b)) 

        realColor = (round(avgColor[0]),round(avgColor[1]),round(avgColor[2]))
        #print(realColor)
        realColorHue = math.atan2(math.sqrt(3)*(float(realColor[1])-float(realColor[2])),2*float(realColor[0])-float(realColor[1])-float(realColor[2])) 
        hueDiff = abs(realColorHue - colorHues[0])
        nearestColor = nearestColors[0]
        nearestIndex = 0

        for i in range(len(nearestColorNodes)):
            hd = abs(realColorHue - colorHues[i])
            #print (realColorHue,':',colorHues[i])
            if (hd < hueDiff):
                hueDiff = hd
                nearestColor = nearestColors[i]
                nearestIndex = i
                                  
        #nearestColor = nearestColors[0]
        if (Canvas.PlacePixel(n[0],n[1],nearestColor) == 1):
            Canvas.ci += 1
            deactivateColorNode(nearestColorNodes[nearestIndex])
            
            #Randomize how things are added to Q
            direction = [1,2,3,4]
            random.shuffle(direction)
            for i in direction:
                if (i == 1):
                    if ((n[0] + 1) < Canvas.xLen):
                        if (not ProcessedCoords[n[0]+1][n[1]]):
                            Q.append((n[0]+1,n[1]))
                elif (i == 2):
                    if ((n[0] - 1) >= 0):
                        if (not ProcessedCoords[n[0]-1][n[1]]):
                            Q.append((n[0]-1,n[1]))
                elif(i == 3):
                    if ((n[1] + 1) < Canvas.yLen):
                        if (not ProcessedCoords[n[0]][n[1]+1]):
                            Q.append((n[0],n[1]+1))

                elif (i == 4):
                    if ((n[1] - 1) >= 0):
                        if (not ProcessedCoords[n[0]][n[1]-1]):
                            Q.append((n[0],n[1]-1))
