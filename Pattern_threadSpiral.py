import AllRGBImageCanvas
import math
import random

canvas = AllRGBImageCanvas.AllRGBImageCanvas(8,'Hue',30000)

numDegrees = 8
thickness = 20

for k in range(4):
    numDegrees = numDegrees*2
    print('NumDegrees = ',numDegrees)
    radius = canvas.xLen/2

    circlePoints = [[0,0] for i in range(numDegrees)]

    for i in range(numDegrees):
        radius = int(canvas.xLen/2)-1
        offset = radius
        #radius = int(math.sqrt(math.pow(xLen,2)+math.pow(yLen,2))/2)
        #offset = int(xLen/2)
        circlePoints[i] = [int(radius*math.cos(2*math.pi*i/numDegrees)+offset),int(radius*math.sin(2*math.pi*i/numDegrees)+offset)]

    for j in range(1,int(numDegrees/2)):
        for i in range(0,numDegrees):
            starting = i
            ending = (starting + int(numDegrees/2)-j)%numDegrees
            #AllRGBImageCanvas.Bresenham(canvas,circlePoints[starting][0],circlePoints[starting][1],circlePoints[ending][0],circlePoints[ending][1])
            AllRGBImageCanvas.DrawLine(canvas, circlePoints[starting][0],circlePoints[starting][1],circlePoints[ending][0],circlePoints[ending][1],thickness)



#Fill the corners
AllRGBImageCanvas.FloodFill_Stack(canvas,0,0)
AllRGBImageCanvas.FloodFill_Stack(canvas,canvas.xLen-1,0)
AllRGBImageCanvas.FloodFill_Stack(canvas,0,canvas.yLen-1)
AllRGBImageCanvas.FloodFill_Stack(canvas,canvas.xLen-1,canvas.yLen-1)

#Fill Center
AllRGBImageCanvas.FloodFill_Stack(canvas,int(canvas.xLen/2),int(canvas.yLen/2))

# for x in range(canvas.xLen):
# 	for y in range (canvas.yLen):
# 		AllRGBImageCanvas.FloodFill_Stack(canvas,x,y)

# while (canvas.ci < canvas.totalPixels):
#     x = random.randint(0,canvas.xLen)
#     y = random.randint(0,canvas.yLen)
#     AllRGBImageCanvas.FloodFill_Stack(canvas,x,y)

canvas.SaveCanvas(str(canvas.ci)+'.bmp')
