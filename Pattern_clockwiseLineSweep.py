import AllRGBImageCanvas
import ColorTree
import math
import random

colorBits = 8
neighborDistance = 10
availableColors = ColorTree.root([],colorBits)
canvas = AllRGBImageCanvas.AllRGBImageCanvas(colorBits,'other',30000)

xind = canvas.xLen/2
yind = canvas.yLen/2

state = 0

x1 = canvas.xLen - 1
y1 = 0

while (availableColors.active):
    AllRGBImageCanvas.Bresenham(canvas, availableColors, int(canvas.xLen/2),int(canvas.yLen/2),x1,y1,5)
    if (state == 0):
        if ((y1+1) < canvas.yLen):
            y1 = y1 + 1
        else:
            state = 1

    if (state == 1):
        if (x1 > 0):
            x1 = x1 - 1
        else:
            state = 2

    if (state == 2):
        if (y1 > 0):
            y1 = y1-1
        else:
            state = 3

    if (state == 3):
        if ((x1+1) < canvas.xLen):
            x1 = x1+1
        else:
            state = 0

canvas.ShowCanvas()
canvas.SaveCanvas(str(canvas.ci)+'.bmp')
